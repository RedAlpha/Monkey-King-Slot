SanZang.png
size:2048,1024
filter:Linear,Linear
pma:true
Body
bounds:886,309,1014,183
offsets:476,959,1793,1210
Head
bounds:2,331,675,882
rotate:90
L Rope
bounds:905,224,83,834
rotate:90
L Rope S
bounds:345,209,98,558
offsets:16,333,114,895
rotate:90
Neck
bounds:2,2,327,341
offsets:4,160,331,501
rotate:90
Necklace
bounds:1912,312,694,133
offsets:63,536,757,669
rotate:90
R Rope
bounds:905,129,93,806
offsets:0,0,94,806
rotate:90
R Rope S
bounds:1902,176,71,134
offsets:1,254,76,388
SanzhangBG
bounds:886,494,1024,512
