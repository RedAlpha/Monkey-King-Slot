﻿#if UNITY_EDITOR
using SimpleJSON;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ClientDataEditorWindow : OdinEditorWindow
{
    [MenuItem("Game Data Editor/Client Data", false, 2)]

    private static void OpenWindow() {
        var window = GetWindow<ClientDataEditorWindow>();
        window.position = GUIHelper.GetEditorWindowRect().AlignCenter(285, 800);
	}

	[BoxGroup("Commands", Order = 0)]
	[HorizontalGroup("Commands/Split", 0.3f, MaxWidth = 270)]
	[HorizontalGroup("Commands/Split/Right")]
	[Button(ButtonSizes.Large), GUIColor(1, 0.2f, 1)]
	[LabelText("Initialize")]
	[EnableIf("NotInitialized", true)]
	public void InitializeButton() {
		Load();
		NotInitialized = false;
	}

	[HorizontalGroup("Commands/Split/Right")]
	[Button(ButtonSizes.Large), GUIColor(1, 0.2f, 1)]
	[LabelText("Revert")]
	[EnableIf("IsValueChanged", true)]
	public void LoadButton() {
		Load();
	}

	[HorizontalGroup("Commands/Split/Right")]
	[Button(ButtonSizes.Large), GUIColor(0, 1f, 0)]
	[LabelText("Save")]
	[EnableIf("IsValueChanged", Value = false)]
	public void SaveButton() {
		Save();
	}

	[BoxGroup("General", Order = 1)]
	[HorizontalGroup("General/Split", 0.98f)]
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float SlotSpeed;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float TimeBeforeSkipAvailable;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float PostRollDelayTime;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public int WinPopUp1BetMultiplier;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public int WinPopUp2BetMultiplier;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged", true)] public List<int> RowCount;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged", true)] public List<int> AutoplayCount;
	[BoxGroup("Nudge Setting", Order = 2)]
	[HorizontalGroup("Nudge Setting/Split", 0.98f)]
	[VerticalGroup("Nudge Setting/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float StartNudgeTime;
	[VerticalGroup("Nudge Setting/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float StartNudgeSpeedMultiplier;
	[VerticalGroup("Nudge Setting/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float StopNudgeLerpSpeed;
	[VerticalGroup("Nudge Setting/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float StopNudgeLerpDistance;
	[VerticalGroup("Nudge Setting/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float StopNudgeSpeedMultiplier;

	private bool IsValueChanged = false;
	private bool NotInitialized = true;

	private void ValueChanged() {
		IsValueChanged = true;
	}

	private void ValueSet() {
		IsValueChanged = false;
	}

	private void Load() {
		JSONNode data = JSON.Parse(Resources.Load<TextAsset>($"Data/Client Data").ToString());

		GetFloat(data, "SlotSpeed", out SlotSpeed);
		GetFloat(data, "TimeBeforeSkipAvailable", out TimeBeforeSkipAvailable);
		GetFloat(data, "PostRollDelayTime", out PostRollDelayTime);
		GetInt(data, "WinPopUp1BetMultiplier", out WinPopUp1BetMultiplier);
		GetInt(data, "WinPopUp2BetMultiplier", out WinPopUp2BetMultiplier);
		GetIntArray(data, "RowCount", out RowCount);
		GetIntArray(data, "AutoplayCount", out AutoplayCount);
		GetFloat(data, "StartNudgeTime", out StartNudgeTime);
		GetFloat(data, "StartNudgeSpeedMultiplier", out StartNudgeSpeedMultiplier);
		GetFloat(data, "StopNudgeLerpSpeed", out StopNudgeLerpSpeed);
		GetFloat(data, "StopNudgeLerpDistance", out StopNudgeLerpDistance);
		GetFloat(data, "StopNudgeSpeedMultiplier", out StopNudgeSpeedMultiplier);

		AssetDatabase.Refresh();
		ValueSet();
	}

	private void Save() {
		JSONObject json = new JSONObject();

		SetFloat(SlotSpeed, "SlotSpeed", ref json);
		SetFloat(TimeBeforeSkipAvailable, "TimeBeforeSkipAvailable", ref json);
		SetFloat(PostRollDelayTime, "PostRollDelayTime", ref json);
		SetInt(WinPopUp1BetMultiplier, "WinPopUp1BetMultiplier", ref json);
		SetInt(WinPopUp2BetMultiplier, "WinPopUp2BetMultiplier", ref json);
		SetIntArray(RowCount, "RowCount", ref json);
		SetIntArray(AutoplayCount, "AutoplayCount", ref json);
		SetFloat(StartNudgeTime, "StartNudgeTime", ref json);
		SetFloat(StartNudgeSpeedMultiplier, "StartNudgeSpeedMultiplier", ref json);
		SetFloat(StopNudgeLerpSpeed, "StopNudgeLerpSpeed", ref json);
		SetFloat(StopNudgeLerpDistance, "StopNudgeLerpDistance", ref json);
		SetFloat(StopNudgeSpeedMultiplier, "StopNudgeSpeedMultiplier", ref json);

		SaveFile($"Assets/Resources/Data", "Client Data", json.ToString());
		AssetDatabase.Refresh();
		ValueSet();
	}

	private void SaveFile(string path, string filename, string json) {
		string completePath = Path.Combine(path, filename + ".json");

		if (File.Exists(completePath)) {
			File.WriteAllText(completePath, json.ToString());
		} else {
			if (!Directory.Exists(path)) {
				Directory.CreateDirectory(path);
				AssetDatabase.Refresh();
			}

			path = EditorUtility.SaveFilePanel("Save object as", path, filename, "json");

			if (!string.IsNullOrEmpty(path) && PathUtilities.TryMakeRelative(Path.GetDirectoryName(Application.dataPath), path, out path)) {
				using (StreamWriter outfile = new StreamWriter(path)) {
					outfile.Write(json);
				}
			}
		}
	}

	private void GetFloat(JSONNode data, string status, out float result) {
		result = data[status].AsFloat;
	}

	private void GetInt(JSONNode data, string status, out int result) {
		result = data[status].AsInt;
	}

	private void GetIntArray(JSONNode data, string status, out List<int> result) {
		if (data[status].IsArray) {
			JSONArray arr = data[status].AsArray;
			result = new List<int>();
			for (int i = 0; i < arr.Count; i++) {
				result.Add(arr[i].AsInt);
			}
		} else {
			result = new List<int>();
			result.Add(data[status].AsInt);
		}
	}

	private void SetFloat(float data, string status, ref JSONObject result) {
		result[status] = data;
	}

	private void SetInt(int data, string status, ref JSONObject result) {
		result[status] = data;
	}

	private void SetIntArray(List<int> array, string status, ref JSONObject result, bool smartConvert = true) {
		if (array.Count > 1) {
			JSONArray ints = new JSONArray();
			for (int i = 0; i < array.Count; i++) {
				ints.Add(array[i]);
			}
			result.Add(status, ints);
		} else if (smartConvert) {
			if (array.Count == 1) {
				result.Add(status, array[0]);
			} else {
				JSONArray ints = new JSONArray();
				result.Add(status, null);
			}
		} else {
			JSONArray ints = new JSONArray();
			if (array.Count == 1) {
				ints.Add(array[0]);
			}
			result.Add(status, ints);
		}
	}
}
#endif