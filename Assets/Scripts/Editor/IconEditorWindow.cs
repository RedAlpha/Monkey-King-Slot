﻿#if UNITY_EDITOR
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using System;
using System.Linq;
using UnityEditor;

public class IconEditorWindow : OdinMenuEditorWindow
{
    [MenuItem("Game Data Editor/Icon", false, 21)]

    private static void OpenWindow() {
        var window = GetWindow<IconEditorWindow>();
        window.position = GUIHelper.GetEditorWindowRect().AlignCenter(850, 720);
	}

    protected override OdinMenuTree BuildMenuTree() {
        OdinMenuTree tree = new OdinMenuTree();
        tree.DefaultMenuStyle.IconSize = 28.00f;
        tree.Config.DrawSearchToolbar = true;

        foreach (IconID icon in Enum.GetValues(typeof(IconID))) {
            if (icon == IconID.FullWild) continue;
            IconScriptableObject iconScriptableObject = new IconScriptableObject();
            tree.Add(icon.ToString(), iconScriptableObject);
        }

        tree.Selection.SelectionChanged += SelectionChanged;

        return tree;
    }

    private void SelectionChanged(SelectionChangedType obj) {
        OdinMenuItem selected = MenuTree.Selection.FirstOrDefault();
        if (selected == null) return;
        if (selected.Value is IconScriptableObject item) {
            IconID id;
            if (Enum.TryParse(selected.Name, out id)) {
                if (item.ID != id || item.ID == 0) {
                    selected.Value = new IconScriptableObject(id);
                }
            }
        }
    }
}
#endif