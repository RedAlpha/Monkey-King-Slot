﻿using SimpleJSON;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class IconScriptableObject : BaseScriptableObject
{
	[BoxGroup("General")]
	[HorizontalGroup("General/Split", 0.4f, LabelWidth = 135)]
	[VerticalGroup("General/Split/Left")] [ReadOnly] public IconID ID;
	[VerticalGroup("General/Split/Left")] [OnValueChanged("ValueChanged")] public int DefaultFrequency;
	[VerticalGroup("General/Split/Left")] [OnValueChanged("ValueChanged")] public int FreeSpinFrequency;
	[VerticalGroup("General/Split/Left")] [OnValueChanged("ValueChanged", true)] public List<int> BetMultiplier;

	public IconScriptableObject() { }

	public IconScriptableObject(IconID iconID) {
		ID = iconID;
		Load();
		Save();
	}

	public override void Load() {
		JSONNode data = JSON.Parse(Resources.Load<TextAsset>($"Data/Icon/{ID}").ToString());

		GetInt(data, "DefaultFrequency", out DefaultFrequency);
		GetInt(data, "FreeSpinFrequency", out FreeSpinFrequency);
		GetIntArray(data, "BetMultiplier", out BetMultiplier);

		AssetDatabase.Refresh();
		ValueSet();
	}

	public override void Save() {
		JSONObject json = new JSONObject();

		SetInt(DefaultFrequency, "DefaultFrequency", ref json);
		SetInt(FreeSpinFrequency, "FreeSpinFrequency", ref json);
		SetIntArray(BetMultiplier, "BetMultiplier", ref json);

		SaveFile($"Assets/Resources/Data/Icon", ID.ToString(), json.ToString());
		AssetDatabase.Refresh();
		ValueSet();
	}
}