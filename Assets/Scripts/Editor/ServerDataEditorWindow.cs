﻿#if UNITY_EDITOR
using SimpleJSON;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ServerDataEditorWindow : OdinEditorWindow
{
    [MenuItem("Game Data Editor/Server Data", false, 1)]

    private static void OpenWindow() {
        var window = GetWindow<ServerDataEditorWindow>();
        window.position = GUIHelper.GetEditorWindowRect().AlignCenter(285, 800);
	}

	[BoxGroup("Commands", Order = 0)]
	[HorizontalGroup("Commands/Split", 0.3f, MaxWidth = 270)]
	[HorizontalGroup("Commands/Split/Right")]
	[Button(ButtonSizes.Large), GUIColor(1, 0.2f, 1)]
	[LabelText("Initialize")]
	[EnableIf("NotInitialized", true)]
	public void InitializeButton() {
		Load();
		NotInitialized = false;
	}

	[HorizontalGroup("Commands/Split/Right")]
	[Button(ButtonSizes.Large), GUIColor(1, 0.2f, 1)]
	[LabelText("Revert")]
	[EnableIf("IsValueChanged", true)]
	public void LoadButton() {
		Load();
	}

	[HorizontalGroup("Commands/Split/Right")]
	[Button(ButtonSizes.Large), GUIColor(0, 1f, 0)]
	[LabelText("Save")]
	[EnableIf("IsValueChanged", Value = false)]
	public void SaveButton() {
		Save();
	}

	[BoxGroup("General", Order = 1)]
	[HorizontalGroup("General/Split", 0.98f)]
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public bool ActivateRTP;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public int HitFrequency;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public int RTP;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public int GridCount;
	[VerticalGroup("General/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public int FullWildChance;
	[BoxGroup("Bet Setting", Order = 2)]
	[HorizontalGroup("Bet Setting/Split", 0.98f)]
	[VerticalGroup("Bet Setting/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public int BetMultiplier;
	[VerticalGroup("Bet Setting/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged", true)] public List<int> BetRange;
	[VerticalGroup("Bet Setting/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged", true)] public List<float> CoinValue;
	[BoxGroup("Bet Multiplier", Order = 3)]
	[HorizontalGroup("Bet Multiplier/Split", 0.98f)]
	[VerticalGroup("Bet Multiplier/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float FreeSpinMultiplier;
	[VerticalGroup("Bet Multiplier/Split/Left"), LabelWidth(180), OnValueChanged("ValueChanged")] public float AnteBetMultiplier;

	private bool IsValueChanged = false;
	private bool NotInitialized = true;

	private void ValueChanged() {
		IsValueChanged = true;
	}

	private void ValueSet() {
		IsValueChanged = false;
	}

	private void Load() {
		JSONNode data = JSON.Parse(Resources.Load<TextAsset>($"Data/Server Data").ToString());

		GetBool(data, "ActivateRTP", out ActivateRTP);
		GetInt(data, "HitFrequency", out HitFrequency);
		GetInt(data, "RTP", out RTP);
		GetInt(data, "GridCount", out GridCount);
		GetInt(data, "FullWildChance", out FullWildChance);
		GetInt(data, "BetMultiplier", out BetMultiplier);
		GetIntArray(data, "BetRange", out BetRange);
		GetFloatArray(data, "CoinValue", out CoinValue);
		GetFloat(data, "FreeSpinMultiplier", out FreeSpinMultiplier);
		GetFloat(data, "AnteBetMultiplier", out AnteBetMultiplier);

		AssetDatabase.Refresh();
		ValueSet();
	}

	private void Save() {
		JSONObject json = new JSONObject();

		SetBool(ActivateRTP, "ActivateRTP", ref json);
		SetInt(HitFrequency, "HitFrequency", ref json);
		SetInt(RTP, "RTP", ref json);
		SetInt(GridCount, "GridCount", ref json);
		SetInt(FullWildChance, "FullWildChance", ref json);
		SetInt(BetMultiplier, "BetMultiplier", ref json);
		SetIntArray(BetRange, "BetRange", ref json);
		SetFloatArray(CoinValue, "CoinValue", ref json);
		SetFloat(FreeSpinMultiplier, "FreeSpinMultiplier", ref json);
		SetFloat(AnteBetMultiplier, "AnteBetMultiplier", ref json);

		SaveFile($"Assets/Resources/Data", "Server Data", json.ToString());
		AssetDatabase.Refresh();
		ValueSet();
	}

	private void SaveFile(string path, string filename, string json) {
		string completePath = Path.Combine(path, filename + ".json");

		if (File.Exists(completePath)) {
			File.WriteAllText(completePath, json.ToString());
		} else {
			if (!Directory.Exists(path)) {
				Directory.CreateDirectory(path);
				AssetDatabase.Refresh();
			}

			path = EditorUtility.SaveFilePanel("Save object as", path, filename, "json");

			if (!string.IsNullOrEmpty(path) && PathUtilities.TryMakeRelative(Path.GetDirectoryName(Application.dataPath), path, out path)) {
				using (StreamWriter outfile = new StreamWriter(path)) {
					outfile.Write(json);
				}
			}
		}
	}

	protected void GetBool(JSONNode data, string status, out bool result) {
		result = data[status].AsBool;
	}

	private void GetInt(JSONNode data, string status, out int result) {
		result = data[status].AsInt;
	}

	private void GetFloat(JSONNode data, string status, out float result) {
		result = data[status].AsFloat;
	}

	private void GetIntArray(JSONNode data, string status, out List<int> result) {
		if (data[status].IsArray) {
			JSONArray arr = data[status].AsArray;
			result = new List<int>();
			for (int i = 0; i < arr.Count; i++) {
				result.Add(arr[i].AsInt);
			}
		} else {
			result = new List<int>();
			result.Add(data[status].AsInt);
		}
	}

	private void GetFloatArray(JSONNode data, string status, out List<float> result) {
		if (data[status].IsArray) {
			JSONArray arr = data[status].AsArray;
			result = new List<float>();
			for (int i = 0; i < arr.Count; i++) {
				result.Add(arr[i].AsFloat);
			}
		} else {
			result = new List<float>();
			result.Add(data[status].AsFloat);
		}
	}

	protected void SetBool(bool data, string status, ref JSONObject result) {
		result[status] = data;
	}

	private void SetInt(int data, string status, ref JSONObject result) {
		result[status] = data;
	}

	private void SetFloat(float data, string status, ref JSONObject result) {
		result[status] = data;
	}

	private void SetIntArray(List<int> array, string status, ref JSONObject result, bool smartConvert = true) {
		if (array.Count > 1) {
			JSONArray ints = new JSONArray();
			for (int i = 0; i < array.Count; i++) {
				ints.Add(array[i]);
			}
			result.Add(status, ints);
		} else if (smartConvert) {
			if (array.Count == 1) {
				result.Add(status, array[0]);
			} else {
				JSONArray ints = new JSONArray();
				result.Add(status, null);
			}
		} else {
			JSONArray ints = new JSONArray();
			if (array.Count == 1) {
				ints.Add(array[0]);
			}
			result.Add(status, ints);
		}
	}

	private void SetFloatArray(List<float> array, string status, ref JSONObject result, bool smartConvert = true) {
		if (array.Count > 1) {
			JSONArray floats = new JSONArray();
			for (int i = 0; i < array.Count; i++) {
				floats.Add(array[i]);
			}
			result.Add(status, floats);
		} else if (smartConvert) {
			if (array.Count == 1) {
				result.Add(status, array[0]);
			} else {
				JSONArray floats = new JSONArray();
				result.Add(status, null);
			}
		} else {
			JSONArray floats = new JSONArray();
			if (array.Count == 1) {
				floats.Add(array[0]);
			}
			result.Add(status, floats);
		}
	}
}
#endif