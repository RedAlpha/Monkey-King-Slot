﻿using Spine.Unity;
using UnityEngine;

public class AnimatedIcon : MonoBehaviour
{
    private IconID iconID;
    private MeshRenderer meshRenderer;
    private SkeletonAnimation skeletonAnimation;
    private MeshRenderer borderMeshRenderer;
    private SkeletonAnimation borderSkeletonAnimation;

    public void Initialize() {
        meshRenderer = GetComponent<MeshRenderer>();
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        borderMeshRenderer = transform.GetChild(0).GetComponent<MeshRenderer>();
        borderSkeletonAnimation = transform.GetChild(0).GetComponent<SkeletonAnimation>();
    }

    public void SetID(IconID iconID) {
        this.iconID = iconID;
    }

    public void SetAndPlayAnimation() {
        if (iconID == IconID.FullWild) return;
        meshRenderer.enabled = true;
        borderMeshRenderer.enabled = true;
        skeletonAnimation.skeletonDataAsset = Resources.Load<SkeletonDataAsset>($"Spine/{iconID}");
        ReloadSkeletonDataAssetAndComponent(GetComponent<SkeletonRenderer>());
        skeletonAnimation.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
        skeletonAnimation.AnimationState.SetAnimation(0, "Win", false);
        borderSkeletonAnimation.AnimationState.SetAnimation(0, "Win", true);
    }

    public void TurnOff() {
        meshRenderer.enabled = false;
        borderMeshRenderer.enabled = false;
    }

    private void ReloadSkeletonDataAssetAndComponent(SkeletonRenderer component) {
        if (component == null) return;
        SkeletonDataAsset skeletonDataAsset = component.skeletonDataAsset;
        if (skeletonDataAsset != null) {
            foreach (AtlasAssetBase aa in skeletonDataAsset.atlasAssets) {
                if (aa != null) aa.Clear();
            }
            skeletonDataAsset.Clear();
        }
        skeletonDataAsset.GetSkeletonData(true);
        if (!(component.SkeletonDataAsset != null && component.SkeletonDataAsset.GetSkeletonData(quiet: true) != null)) return;

        component.Initialize(true);
        component.LateUpdate();
    }
}
