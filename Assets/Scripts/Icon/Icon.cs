﻿using UnityEngine;
using UnityEngine.U2D;

public class Icon : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private SpriteAtlas iconAtlas;
    private IconID iconID;

    public void Initialize(SpriteAtlas iconAtlas) {
        this.iconAtlas = iconAtlas;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void SetSprite(IconID iconID) {
        this.iconID = iconID;
        // MK-Specific
        if (iconID == IconID.FullWild) {
            this.iconID = (IconID)Random.Range(0, System.Enum.GetValues(typeof(IconID)).Length - 2);
        }
        spriteRenderer.sprite = iconAtlas.GetSprite(this.iconID.ToString());
    }

    public void ToggleSpriteRenderer(bool value) {
        if (iconID == IconID.FullWild) return;
        spriteRenderer.enabled = value;
    }
}
