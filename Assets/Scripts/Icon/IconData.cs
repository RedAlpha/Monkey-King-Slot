﻿using SimpleJSON;
using System.Collections.Generic;
using UnityEngine;

public static class IconData
{
	private static readonly Dictionary<IconID, JSONNode> cachedStatus = new Dictionary<IconID, JSONNode>();
	private static readonly Dictionary<IconID, Dictionary<IconStatus, JSONNode>> cachedNode = new Dictionary<IconID, Dictionary<IconStatus, JSONNode>>();

	private static JSONNode LoadData(IconID iconID) {
		return JSON.Parse(Resources.Load<TextAsset>($"Data/Icon/{iconID.ToString()}").ToString());
	}

	private static JSONNode GetUnitData(IconID iconID) {
		return cachedStatus.TryGetValue(iconID, out JSONNode data) ? data : (cachedStatus[iconID] = LoadData(iconID));
	}

	private static JSONNode Get(IconID iconID, IconStatus key) {
		if (!cachedNode.TryGetValue(iconID, out Dictionary<IconStatus, JSONNode> data)) {
			data = cachedNode[iconID] = new Dictionary<IconStatus, JSONNode>();
		}
		if (!data.TryGetValue(key, out JSONNode parameter)) {
			parameter = data[key] = GetUnitData(iconID)[key.ToString()];
		}
		if (parameter == null) {
			parameter = JSONNull.CreateOrGet();
		}

		return parameter;
	}

	public static string GetString(IconID iconID, IconStatus iconStatus) {
		return Get(iconID, iconStatus).Value;
	}

	public static float GetFloat(IconID iconID, IconStatus iconStatus) {
		return Get(iconID, iconStatus).AsFloat;
	}

	public static int GetInt(IconID iconID, IconStatus iconStatus) {
		return Get(iconID, iconStatus).AsInt;
	}

	public static int[] GetIntArray(IconID iconID, IconStatus iconStatus) {
		JSONArray arr = GetUnitData(iconID)[iconStatus.ToString()].AsArray;
		int count = arr.Count;
		int[] ints = new int[count];
		for (int i = 0; i < count; i++) {
			ints[i] = arr[i].AsInt;
		}
		return ints;
	}

	public static float[] GetFloatArray(IconID iconID, IconStatus iconStatus) {
		if (GetUnitData(iconID)[iconStatus.ToString()].Count == 0)
		{
			float[] floats = new float[1];
			floats[0] = GetUnitData(iconID)[iconStatus.ToString()];
			return floats;
		} else {
			JSONArray arr = GetUnitData(iconID)[iconStatus.ToString()].AsArray;
			int count = arr.Count;
			float[] floats = new float[count];
			for (int i = 0; i < count; i++) {
				floats[i] = arr[i].AsFloat;
			}
			return floats;
		}
	}

	public static bool GetBool(IconID iconID, IconStatus iconStatus) {
		return Get(iconID, iconStatus).AsBool;
	}
}