﻿using SimpleJSON;
using UnityEngine;

namespace RedRain.Slot {
    public static partial class SlotSystem
	{
		public static readonly float SlotSpeed = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["SlotSpeed"];
		public static readonly float TimeBeforeSkipAvailable = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["TimeBeforeSkipAvailable"];
		public static readonly float PostRollDelayTime = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["PostRollDelayTime"];
		public static readonly int WinPopUp1BetMultiplier = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["WinPopUp1BetMultiplier"];
		public static readonly int WinPopUp2BetMultiplier = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["WinPopUp2BetMultiplier"];
		public static readonly JSONArray RowCount = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["RowCount"].AsArray;
		public static readonly JSONArray AutoplayCount = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["AutoplayCount"].AsArray;

		public static readonly float StartNudgeTime = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["StartNudgeTime"];
		public static readonly float StartNudgeSpeedMultiplier = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["StartNudgeSpeedMultiplier"];
		public static readonly float StopNudgeLerpSpeed = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["StopNudgeLerpSpeed"];
		public static readonly float StopNudgeLerpDistance = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["StopNudgeLerpDistance"];
		public static readonly float StopNudgeSpeedMultiplier = JSON.Parse(Resources.Load<TextAsset>("Data/Client Data").ToString())["StopNudgeSpeedMultiplier"];
	}
}