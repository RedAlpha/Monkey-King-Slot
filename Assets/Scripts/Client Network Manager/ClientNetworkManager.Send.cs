﻿using RedRain.Slot;

public partial class ClientNetworkManager
{
    private void SendToServer_Pull() {
        SlotSystem.SimulatedServer.ClientPull();
    }

    private void SendToServer_SlotAnimationFinish() {
        SlotSystem.SimulatedServer.ClientSlotAnimationFinished();
    }

    private void SendToServer_DecreaseBet() {
        SlotSystem.SimulatedServer.ClientDecreaseBet();
    }

    private void SendToServer_IncreaseBet() {
        SlotSystem.SimulatedServer.ClientIncreaseBet();
    }

    private void SendToServer_DecreaseCoinValue() {
        SlotSystem.SimulatedServer.ClientDecreaseCoinValue();
    }

    private void SendToServer_IncreaseCoinValue() {
        SlotSystem.SimulatedServer.ClientIncreaseCoinValue();
    }

    private void SendToServer_DecreaseTotalBet() {
        SlotSystem.SimulatedServer.ClientDecreaseTotalBet();
    }

    private void SendToServer_IncreaseTotalBet() {
        SlotSystem.SimulatedServer.ClientIncreaseTotalBet();
    }

    private void SendToServer_BuyFreeSpin() {
        SlotSystem.SimulatedServer.ClientBuyFreeSpin();
    }

    private void SendToServer_ToggleDoubleChance(bool isDoubleChance) {
        SlotSystem.SimulatedServer.ClientToggleDoubleChance(isDoubleChance);
    }

    private void SendToServer_RequestSyncData() {
        SlotSystem.SimulatedServer.ClientRequestingSyncData();
    }
}
