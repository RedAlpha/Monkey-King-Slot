﻿using RedRain.Slot;

public partial class ClientNetworkManager
{
    private SlotManager slotManager;

    public ClientNetworkManager(SlotManager slotManager) {
        this.slotManager = slotManager;
        slotManager.SendToServerEvent_Pull += SendToServer_Pull;
        slotManager.SendToServerEvent_SlotAnimationFinish += SendToServer_SlotAnimationFinish;
        slotManager.SendToServerEvent_DecreaseBet += SendToServer_DecreaseBet;
        slotManager.SendToServerEvent_IncreaseBet += SendToServer_IncreaseBet;
        slotManager.SendToServerEvent_DecreaseCoinValue += SendToServer_DecreaseCoinValue;
        slotManager.SendToServerEvent_IncreaseCoinValue += SendToServer_IncreaseCoinValue;
        slotManager.SendToServerEvent_DecreaseTotalBet += SendToServer_DecreaseTotalBet;
        slotManager.SendToServerEvent_IncreaseTotalBet += SendToServer_IncreaseTotalBet;
        slotManager.SendToServerEvent_RequestSyncData += SendToServer_RequestSyncData;
        slotManager.SendToServerEvent_BuyFreeSpin += SendToServer_BuyFreeSpin;
        slotManager.SendToServerEvent_ToggleDoubleChance += SendToServer_ToggleDoubleChance;
        SlotSystem.SimulatedServer.SendToClientEvent_InitializationData += SendToClient_InitializationData;
        SlotSystem.SimulatedServer.SendToClientEvent_Pull += SendToClient_Pull;
        SlotSystem.SimulatedServer.SendToClientEvent_SlotAnimationFinish += SendToClient_SlotAnimationFinish;
        SlotSystem.SimulatedServer.SendToClientEvent_BetData += SendToClient_BetData;
        SlotSystem.SimulatedServer.SendToClientEvent_ToggleDoubleChance += SendToClient_ToggleDoubleChance;
    }
}
