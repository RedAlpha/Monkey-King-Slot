﻿using RedRain.Slot;
using System.Collections.Generic;

public partial class ClientNetworkManager
{
    private void SendToClient_Pull(List<IconID> slotContent, PullType nextPullType, float creditMinusWinning) {
        slotManager.ReceivePullSignal(slotContent, nextPullType, creditMinusWinning);
    }

    private void SendToClient_SlotAnimationFinish(List<MatchingResult> matchingResults, PullType pullType, float thisRoundWinning) {
        slotManager.ReceiveSlotAnimationFinished(matchingResults, pullType, thisRoundWinning);
    }

    private void SendToClient_BetData(BetDetail betDetail, float buyFreeSpinBetCost, float doubleChanceBetCost, float credit) {
        slotManager.ReceiveBetData(betDetail, buyFreeSpinBetCost, doubleChanceBetCost, credit);
    }

    private void SendToClient_ToggleDoubleChance(bool isDoubleChance, float normalBet, float doubleChanceBet) {
        slotManager.ReceiveToggleDoubleChance(isDoubleChance, normalBet, doubleChanceBet);
    }

    private void SendToClient_InitializationData(float betMultiplier) {
        slotManager.ReceiveInitializationData(betMultiplier);
    }
}
