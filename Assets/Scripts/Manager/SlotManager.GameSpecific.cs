﻿using RedRain.Slot;
using System.Collections.Generic;
using UnityEngine;

public partial class SlotManager
{
	[SerializeField] private SlotMachineView columnManager;

	private void InitializeGameSpecifics() {
		columnManager.SlotAnimationFinishEvent += OnSlotAnimationFinished;
		uiManager.BuyFreeSpinEvent += OnBuyFreeSpin;
		uiManager.ToggleDoubleChanceEvent += OnToggleDoubleChance;
	}

	private void OnSlotAnimationFinished() {
		FinishSlotAnimation();
	}

	private void OnResultSkippedGameSpecifics() {
		columnManager.SkipResult();
	}

	public void PlayIconAnimations(List<MatchingResult> matchingResults) {
		columnManager.PlayIconAnimations(matchingResults);
	}
}
