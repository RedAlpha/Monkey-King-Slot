using UnityEngine;

public class SoundManager : MonoBehaviour
{
	[Header("BGM")]
	[SerializeField] private GameObject bgmObject;
	[SerializeField] private bool dynamicRollBGM;
	[SerializeField] private AudioSource idleBGM;
	[SerializeField] private AudioSource rollBGM;
	[SerializeField] private AudioSource freeSpinBGM;
	[Header("SFX")]
	[SerializeField] private GameObject sfxObject;
	[SerializeField] private AudioSource startSFX;
	[SerializeField] private AudioSource loopSFX;
	[SerializeField] private AudioSource stopSFX;
	[SerializeField] private AudioSource winFreeSpinSFX;
	[SerializeField] private AudioSource winPopUp1SFX;
	[SerializeField] private AudioSource winPopUp2SFX;

	private PullType currentPullType;

	public void OnPullFromServer(PullType pullType) {
		currentPullType = pullType;
		startSFX.Play();
		loopSFX.Play();
		if (dynamicRollBGM && pullType == PullType.Normal) {
			idleBGM.volume = 0;
			rollBGM.volume = 1f;
		}
	}

	public void OnSlotAnimationFinishedFromServer(PullType nextPullType) {
		loopSFX.Stop();
		stopSFX.Play();
		if (nextPullType == PullType.Normal) {
			idleBGM.volume = 1f;
			if (currentPullType == PullType.FreeSpin) {
				freeSpinBGM.Stop();
				idleBGM.Play();
				if (dynamicRollBGM) rollBGM.Play();
			} else if (dynamicRollBGM) {
				rollBGM.volume = 0;
			}
		} else if (nextPullType == PullType.FreeSpin) {
			if (currentPullType == PullType.Normal) {
				idleBGM.Stop();
				idleBGM.volume = 0;
				if (dynamicRollBGM) {
					rollBGM.Stop();
					rollBGM.volume = 0;
				}
				freeSpinBGM.Play();
			}
		}
	}

	public void ToggleBGM(bool value) {
		bgmObject.SetActive(value);
	}

	public void ToggleSFX(bool value) {
		sfxObject.SetActive(value);
	}

	public void PlayWinFreeSpinSFX() {
		winFreeSpinSFX.Play();
	}

	public void PlayWinPopUp1SFX() {
		winPopUp1SFX.Play();
	}

	public void PlayWinPopUp2SFX() {
		winPopUp2SFX.Play();
	}
}
