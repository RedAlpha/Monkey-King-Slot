﻿using UnityEngine;
using UnityEngine.UI;

public partial class UIManager : MonoBehaviour
{
	[Header("Setting")]
	[SerializeField] private Button settingButton;
	[SerializeField] private GameObject settingWindow;
	[SerializeField] private Toggle settingTurboSpinToggle;

	private void OpenSettingWindow() {
		settingTurboSpinToggle.isOn = GameClientOption.IsTurbo;
		settingWindow.SetActive(true);
	}
}
