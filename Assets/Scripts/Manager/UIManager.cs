﻿using RedRain.Slot;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public partial class UIManager : MonoBehaviour
{
	[Header("Main Buttons")]
	[SerializeField] private GameObject pullButton;
	[SerializeField] private GameObject skipButton;
	[SerializeField] private Button autoplayButton;
	[SerializeField] private Button totalBetPlusMainButton;
	[SerializeField] private Button totalBetMinusMainButton;
	[Header("Info Corner")]
	[SerializeField] private TextMeshProUGUI creditText;
	[SerializeField] private TextMeshProUGUI mainBetText;
	[Header("Winning Text")]
	[SerializeField] private TextMeshProUGUI winAmountText;
	[SerializeField] private TextMeshProUGUI winPaylineText;
	[Header("Pop Up Game UI")]
	[SerializeField] private GameObject winFreeSpinPopUp;
	[SerializeField] private GameObject Win1PopUpObject;
	[SerializeField] private GameObject Win2PopUpObject;
	[SerializeField] private TextMeshProUGUI winFreeSpinCountText;
	[SerializeField] private TextMeshProUGUI winPopUp1AmountText;
	[SerializeField] private TextMeshProUGUI winPopUp2AmountText;
	[Header("Buy Free Spin")]
	[SerializeField] private GameObject freeSpinConfirmationWindow;
	[SerializeField] private TextMeshProUGUI buyFreeSpinCostText;
	[SerializeField] private TextMeshProUGUI buyFreeSpinConfirmationCostText;
	[SerializeField] private Button buyFreeSpinCostButton;
	[Header("Toggle Double Chance")]
	[SerializeField] private TextMeshProUGUI doubleChanceCostText;
	[SerializeField] private Toggle doubleChanceToggle;
	public Action PullSlotEvent;
	public Action DecreaseBetEvent;
	public Action IncreaseBetEvent;
	public Action DecreaseCoinValueEvent;
	public Action IncreaseCoinValueEvent;
	public Action DecreaseTotalBetEvent;
	public Action IncreaseTotalBetEvent;
	public Action BuyFreeSpinEvent;
	public Action<bool> ToggleDoubleChanceEvent;
	// Client-side UI Events
	public Action SkipResultEvent;
	public Action CloseFreeSpinPopUpEvent;
	public Action<bool> ToggleBGMEvent;
	public Action<bool> ToggleSFXEvent;

	public void InitializeUI() {
		mainPullButton = pullButton.GetComponent<Button>();
	}

	void Update() {
		UpdateCreditText();
	}
}