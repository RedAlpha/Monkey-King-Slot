﻿using RedRain.Slot;

public partial class UIManager
{
	private int freeSpinRemaining;

	public void OnPullFromServer(PullType pullType, float creditMinusWinning) {
		SetCreditText(creditMinusWinning);
		if (pullType == PullType.FreeSpin) {
			freeSpinRemaining--;
			StartCoroutine(ReplacePullButton());
			winPaylineText.text = $"Free Spin Remaining : {freeSpinRemaining}";
		}
	}

	public void OnSlotAnimationFinishedFromServer(PullType nextPullType, float thisRoundWinning, int matchingCount) {
		ReplaceSkipButton();
		StartCoroutine(ProcessSlotAnimationFinished(nextPullType, matchingCount > 0 ? SlotSystem.PostRollDelayTime : 0));
		if (matchingCount > 0) {
			winAmountText.text = $"WIN : Rp.{thisRoundWinning},00";
			winPaylineText.text = $"{matchingCount} Paylines";
			if (matchingCount > 1) {
				winPaylineText.text = $"{matchingCount} Paylines";
			} else {
				winPaylineText.text = $"{matchingCount} Payline";
			}
		}
	}

	public void OnBetChangedFromServer(BetDetail betDetail, float buyFreeSpinBetCost, float doubleChanceBetCost, float credit, bool isDoubleChance) {
		AdjustBet(betDetail, isDoubleChance, doubleChanceBetCost);
		SetCreditText(credit);
		buyFreeSpinCostText.text = $"Rp.{buyFreeSpinBetCost},00";
		buyFreeSpinConfirmationCostText.text = $"Rp.{buyFreeSpinBetCost},00";
		buyFreeSpinCostButton.interactable = buyFreeSpinBetCost <= credit && remainingAutoplayCount <= 0;
		doubleChanceCostText.text = $"Rp.{doubleChanceBetCost},00";
		doubleChanceToggle.interactable = doubleChanceBetCost <= credit && remainingAutoplayCount <= 0;
	}

	public void OnToggleDoubleChanceFromServer(bool isDoubleChance, float normalBet, float doubleChanceBet) {
		mainBetText.text = $"Rp.{(isDoubleChance ? doubleChanceBet : normalBet)},00";
	}
}
