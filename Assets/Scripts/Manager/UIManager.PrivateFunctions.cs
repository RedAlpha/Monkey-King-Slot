﻿using RedRain.Slot;
using System.Collections;
using UnityEngine;

public partial class UIManager : MonoBehaviour
{
	private void ReplaceSkipButton() {
		skipButton.SetActive(false);
		pullButton.SetActive(true);
	}

	private IEnumerator ReplacePullButton() {
		winAmountText.text = "";
		winPaylineText.text = "";
		mainPullButton.interactable = false;
		buyFreeSpinCostButton.interactable = false;
		doubleChanceToggle.interactable = false;
		totalBetPlusMainButton.interactable = false;
		totalBetMinusMainButton.interactable = false;
		settingButton.interactable = false;
		autoplayButton.interactable = false;
		CloseBetDetails();
		yield return new WaitForSeconds(SlotSystem.TimeBeforeSkipAvailable);
		if (GameClientOption.IsTurbo) {
			SkipResult();
		} else {
			pullButton.SetActive(false);
			skipButton.SetActive(true);
		}
	}

	private IEnumerator ProcessSlotAnimationFinished(PullType nextPullType, float waitTime) {
		if (nextPullType == PullType.FreeSpin) {
			StopAutoplaySequence();
		}
		bool isAutoplay = remainingAutoplayCount > 0 && nextPullType != PullType.FreeSpin;
		yield return new WaitForSeconds(isAutoplay ? SlotSystem.PostRollDelayTime : waitTime);
		if (isAutoplay) {
			PullSlotMachine();
			remainingAutoplayCount--;
			winPaylineText.text = $"Remaining Autoplay : {remainingAutoplayCount}";
		} else {
			bool isInteractable = nextPullType != PullType.FreeSpin;
			mainPullButton.interactable = isInteractable;
			settingButton.interactable = isInteractable;
			autoplayButton.interactable = isInteractable;
		}
	}
}
