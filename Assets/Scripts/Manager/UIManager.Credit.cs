﻿using UnityEngine;

public partial class UIManager : MonoBehaviour
{
	private float initialCreditValue, targetCreditValue, currentCreditValue;
	private float timePassed;
	private float timeNeeded = 0.7f;
	private bool isUpdating = false;

	private void SetCreditText(float credit) {
		targetCreditValue = credit;
		initialCreditValue = currentCreditValue;
		timePassed = 0;
		isUpdating = true;
	}

	private void UpdateCreditText() {
		if (!isUpdating) return;

		currentCreditValue = Mathf.Lerp(initialCreditValue, targetCreditValue, timePassed);
		timePassed += 1f / timeNeeded * Time.deltaTime;
		if (timePassed >= 1f) {
			isUpdating = false;
			currentCreditValue = targetCreditValue;
		}
		creditText.text = $"Rp.{currentCreditValue},00";
	}
}