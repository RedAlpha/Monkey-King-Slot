﻿using RedRain.Slot;
using UnityEngine;

public partial class SlotManager : MonoBehaviour
{
	private UIManager uiManager;
	private SoundManager soundManager;
	private bool isFreeSpinPopUpClosed = false;

	void Start() {
		SlotSystem.InitializeSystem();
		new ClientNetworkManager(this);
		uiManager = FindObjectsOfType<UIManager>()[0];
		uiManager.InitializeUI();
		uiManager.PullSlotEvent += OnSlotPulled;
		uiManager.DecreaseBetEvent += OnBetDecreased;
		uiManager.IncreaseBetEvent += OnBetIncreased;
		uiManager.DecreaseCoinValueEvent += OnCoinValueDecreased;
		uiManager.IncreaseCoinValueEvent += OnCoinValueIncreased;
		uiManager.DecreaseTotalBetEvent += OnTotalBetDecreased;
		uiManager.IncreaseTotalBetEvent += OnTotalBetIncreased;
		// Client-side UI Events
		uiManager.SkipResultEvent += OnResultSkipped;
		uiManager.CloseFreeSpinPopUpEvent += OnFreeSpinPopUpClosed;
		uiManager.ToggleBGMEvent += OnBGMToggled;
		uiManager.ToggleSFXEvent += OnSFXToggled;
		soundManager = FindObjectsOfType<SoundManager>()[0];
		SlotSystem.SimulatedServer.SetupMockData();
		InitializeGameSpecifics();
	}

	private void OnResultSkipped() { OnResultSkippedGameSpecifics(); }

	private void OnFreeSpinPopUpClosed() { isFreeSpinPopUpClosed = true; }

	private void OnBGMToggled(bool value) {
		soundManager.ToggleBGM(value);
	}

	private void OnSFXToggled(bool value) {
		soundManager.ToggleSFX(value);
	}
}
