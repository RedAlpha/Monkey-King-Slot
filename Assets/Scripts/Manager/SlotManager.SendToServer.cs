﻿using System;

public partial class SlotManager
{
	public Action SendToServerEvent_Pull;
	public Action SendToServerEvent_SlotAnimationFinish;
	public Action SendToServerEvent_DecreaseBet;
	public Action SendToServerEvent_IncreaseBet;
	public Action SendToServerEvent_DecreaseCoinValue;
	public Action SendToServerEvent_IncreaseCoinValue;
	public Action SendToServerEvent_DecreaseTotalBet;
	public Action SendToServerEvent_IncreaseTotalBet;
	public Action SendToServerEvent_BuyFreeSpin;
	public Action<bool> SendToServerEvent_ToggleDoubleChance;
	public Action SendToServerEvent_RequestSyncData;

	private void OnSlotPulled() { SendToServerEvent_Pull?.Invoke(); }
	private void FinishSlotAnimation() { SendToServerEvent_SlotAnimationFinish?.Invoke(); }
	private void OnBetDecreased() { SendToServerEvent_DecreaseBet?.Invoke(); }
	private void OnBetIncreased() { SendToServerEvent_IncreaseBet?.Invoke(); }
	private void OnCoinValueDecreased() { SendToServerEvent_DecreaseCoinValue?.Invoke(); }
	private void OnCoinValueIncreased() { SendToServerEvent_IncreaseCoinValue?.Invoke(); }
	private void OnTotalBetDecreased() { SendToServerEvent_DecreaseTotalBet?.Invoke(); }
	private void OnTotalBetIncreased() { SendToServerEvent_IncreaseTotalBet?.Invoke(); }
	private void OnBuyFreeSpin() { SendToServerEvent_BuyFreeSpin?.Invoke(); }
	private void OnToggleDoubleChance(bool isDoubleChance) { SendToServerEvent_ToggleDoubleChance?.Invoke(isDoubleChance); }
}