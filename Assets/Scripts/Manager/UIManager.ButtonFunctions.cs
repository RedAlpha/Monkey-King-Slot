﻿using UnityEngine;

public partial class UIManager : MonoBehaviour
{
	public void PullSlotMachine() {
		StartCoroutine(ReplacePullButton());
		PullSlotEvent?.Invoke();
	}

	public void SkipResult() {
		ReplaceSkipButton();
		SkipResultEvent?.Invoke();
	}

	#region Bet Details Buttons
	public void DecreaseBet() {
		TotalBetMinus();
		OpenBetDetailsWindow();
	}

	public void IncreaseBet() {
		TotalBetPlus();
		OpenBetDetailsWindow();
	}

	public void BetPlus() {
		IncreaseBetEvent?.Invoke();
	}

	public void BetMinus() {
		DecreaseBetEvent?.Invoke();
	}

	public void CoinValuePlus() {
		IncreaseCoinValueEvent?.Invoke();
	}

	public void CoinValueMinus() {
		DecreaseCoinValueEvent?.Invoke();
	}

	public void TotalBetPlus() {
		IncreaseTotalBetEvent?.Invoke();
	}

	public void TotalBetMinus() {
		DecreaseTotalBetEvent?.Invoke();
	}

	public void CloseBetDetails() {
		betDetailsWindow.SetActive(false);
	}
	#endregion

	#region Autoplay Buttons
	public void OpenAutoplay() {
		OpenAutoplayWindow();
	}

	public void IncreaseAutoplayCountButton() {
		IncreaseAutoplayCount();
	}

	public void DecreaseAutoplayCountButton() {
		DecreaseAutoplayCount();
	}

	public void StartAutoplay() {
		StartAutoplaySequence();
	}

	public void StopAutoplay() {
		StopAutoplaySequence();
	}

	public void CloseAutoplay() {
		autoplayWindow.SetActive(false);
	}
	#endregion

	#region Buy Free Spin
	public void BuyFreeSpin() {
		freeSpinConfirmationWindow.SetActive(true);
	}

	public void CancelBuyFreeSpin() {
		freeSpinConfirmationWindow.SetActive(false);
	}

	public void ConfirmBuyFreeSpin() {
		CancelBuyFreeSpin();
		StartCoroutine(ReplacePullButton());
		BuyFreeSpinEvent?.Invoke();
	}
	#endregion

	public void ToggleDoubleChance(bool isDoubleChance) {
		ToggleDoubleChanceEvent?.Invoke(isDoubleChance);
		buyFreeSpinCostButton.interactable = !isDoubleChance;
	}

	#region Setting Buttons
	public void OpenSetting() {
		OpenSettingWindow();
	}

	public void CloseSetting() {
		settingWindow.SetActive(false);
	}

	public void ToggleBGM(bool value) {
		ToggleBGMEvent?.Invoke(value);
	}

	public void ToggleSFX(bool value) {
		ToggleSFXEvent?.Invoke(value);
	}

	public void ToggleTurbo(bool value) {
		GameClientOption.IsTurbo = value;
	}
	#endregion

	#region Pop Up Buttons
	public void CloseFreeSpinPopUp() {
		isClosingPopUpFreeSpin = true;
		CloseFreeSpinPopUpEvent?.Invoke();
	}

	public void CloseBigWinPopUp() {
		isClosingWinPopUp1 = true;
	}

	public void CloseSuperWinPopUp() {
		isClosingWinPopUp2 = true;
	}
	#endregion
}
