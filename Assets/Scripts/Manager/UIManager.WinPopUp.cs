﻿using RedRain.Slot;
using System.Collections;
using UnityEngine;

public partial class UIManager
{
	private bool isClosingPopUpFreeSpin, isClosingWinPopUp1, isClosingWinPopUp2;

	public void ShowFreeSpinPopUp() {
		StartCoroutine(PopUpWinFreeSpin());
		freeSpinRemaining = 10;
	}

	public void ShowWinPopUp(int popUpIndex, float thisRoundWinning) {
		StartCoroutine(PopUpWin(popUpIndex, thisRoundWinning, freeSpinRemaining > 0));
	}

	private IEnumerator PopUpWinFreeSpin() {
		isClosingPopUpFreeSpin = false;
		winFreeSpinPopUp.SetActive(true);
		winFreeSpinCountText.text = "10";
		if (remainingAutoplayCount > 0) {
			yield return new WaitForSeconds(SlotSystem.PostRollDelayTime);
			CloseFreeSpinPopUpEvent?.Invoke();
		} else {
			yield return new WaitUntil(() => isClosingPopUpFreeSpin);
		}
		isClosingPopUpFreeSpin = false;
		winFreeSpinPopUp.SetActive(false);
	}

	private IEnumerator PopUpWin(int popUpIndex, float thisRoundWinning, bool isFreeSpin) {
		switch (popUpIndex) {
			case 1:
				isClosingWinPopUp1 = false;
				Win1PopUpObject.SetActive(true);
				winPopUp1AmountText.text = $"Rp.{thisRoundWinning},00";
				if (remainingAutoplayCount > 0 || isFreeSpin) {
					yield return new WaitForSeconds(SlotSystem.PostRollDelayTime);
				} else {
					yield return new WaitUntil(() => isClosingWinPopUp1);
				}
				isClosingWinPopUp1 = false;
				Win1PopUpObject.SetActive(false);
				break;
			case 2:
				isClosingWinPopUp2 = false;
				Win2PopUpObject.SetActive(true);
				winPopUp2AmountText.text = $"Rp.{thisRoundWinning},00";
				if (remainingAutoplayCount > 0 || isFreeSpin) {
					yield return new WaitForSeconds(SlotSystem.PostRollDelayTime);
				} else {
					yield return new WaitUntil(() => isClosingWinPopUp2);
				}
				isClosingWinPopUp2 = false;
				Win2PopUpObject.SetActive(false);
				break;
		}
	}
}
