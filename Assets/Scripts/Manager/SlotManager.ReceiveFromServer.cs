﻿using RedRain.Slot;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SlotManager
{
	private bool isDoubleChance;
	private float betStoredInClient;
	private PullType cachedClientCurrentPullType;

	public void ReceivePullSignal(List<IconID> slotContent, PullType pullType, float creditMinusWinning) {
		float waitTime = 0;
		if (pullType == PullType.FreeSpin) waitTime = 1.2f;
		StartCoroutine(ClientPullSequence(waitTime, slotContent, pullType, creditMinusWinning));
	}

	public void ReceiveSlotAnimationFinished(List<MatchingResult> matchingResults, PullType nextPullType, float thisRoundWinning) {
		PlayIconAnimations(matchingResults);
		uiManager.OnSlotAnimationFinishedFromServer(nextPullType, thisRoundWinning, matchingResults.Count);
		soundManager.OnSlotAnimationFinishedFromServer(nextPullType);
		if (cachedClientCurrentPullType == PullType.Normal && nextPullType == PullType.FreeSpin) {
			uiManager.ShowFreeSpinPopUp();
			soundManager.PlayWinFreeSpinSFX();
		} else if (thisRoundWinning >= betStoredInClient * SlotSystem.WinPopUp2BetMultiplier) {
			uiManager.ShowWinPopUp(2, thisRoundWinning);
			soundManager.PlayWinPopUp2SFX();
		} else if (thisRoundWinning >= betStoredInClient * SlotSystem.WinPopUp1BetMultiplier) {
			uiManager.ShowWinPopUp(1, thisRoundWinning);
			soundManager.PlayWinPopUp1SFX();
		}
	}

	public void ReceiveBetData(BetDetail betDetail, float buyFreeSpinBetCost, float doubleChanceBetCost, float credit) {
		uiManager.OnBetChangedFromServer(betDetail, buyFreeSpinBetCost, doubleChanceBetCost, credit, isDoubleChance);
		betStoredInClient = betDetail.totalBet;
	}

	public void ReceiveToggleDoubleChance(bool isDoubleChance, float normalBet, float doubleChanceBet) {
		this.isDoubleChance = isDoubleChance;
		uiManager.OnToggleDoubleChanceFromServer(isDoubleChance, normalBet, doubleChanceBet);
	}

	public void ReceiveInitializationData(float betMultiplier) {
		uiManager.SetBetMultiplierText(betMultiplier);
	}

	private IEnumerator ClientPullSequence(float waitTime, List<IconID> slotContent, PullType pullType, float creditMinusWinning) {
		if (pullType == PullType.FreeSpin && cachedClientCurrentPullType == PullType.Normal) {
			yield return new WaitUntil(() => isFreeSpinPopUpClosed);
			isFreeSpinPopUpClosed = false;
		} else {
			yield return new WaitForSeconds(waitTime);
		}
		uiManager.OnPullFromServer(pullType, creditMinusWinning);
		soundManager.OnPullFromServer(pullType);
		columnManager.BeginSlotAnimation(slotContent);
		cachedClientCurrentPullType = pullType;
	}
}
