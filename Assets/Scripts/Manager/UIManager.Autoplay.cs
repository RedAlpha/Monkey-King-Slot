﻿using RedRain.Slot;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public partial class UIManager : MonoBehaviour
{
	[Header("Autoplay")]
	[SerializeField] private GameObject autoplayWindow;
	[SerializeField] private Toggle autoplayTurboSpinToggle;
	[SerializeField] private TextMeshProUGUI autoplayCountText;
	[SerializeField] private GameObject startAutoplayObject;
	[SerializeField] private GameObject stopAutoplayObject;

	private int autoplayCountIndex = 0;
	private int remainingAutoplayCount;

	private void OpenAutoplayWindow() {
		autoplayTurboSpinToggle.isOn = GameClientOption.IsTurbo;
		autoplayCountText.text = SlotSystem.AutoplayCount[autoplayCountIndex];
		autoplayWindow.SetActive(true);
	}

	private void StartAutoplaySequence() {
		remainingAutoplayCount = SlotSystem.AutoplayCount[autoplayCountIndex] - 1;
		startAutoplayObject.SetActive(false);
		stopAutoplayObject.SetActive(true);
		CloseAutoplay();
		PullSlotMachine();
		winPaylineText.text = $"Remaining Autoplay : {remainingAutoplayCount}";
	}

	private void StopAutoplaySequence() {
		remainingAutoplayCount = 0;
		winPaylineText.text = "";
		startAutoplayObject.SetActive(true);
		stopAutoplayObject.SetActive(false);
	}

	private void IncreaseAutoplayCount() {
		if (autoplayCountIndex >= SlotSystem.AutoplayCount.Count - 1) return;
		autoplayCountIndex++;
		autoplayCountText.text = SlotSystem.AutoplayCount[autoplayCountIndex];
	}

	private void DecreaseAutoplayCount() {
		if (autoplayCountIndex <= 0) return;
		autoplayCountIndex--;
		autoplayCountText.text = SlotSystem.AutoplayCount[autoplayCountIndex];
	}
}
