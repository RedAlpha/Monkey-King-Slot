﻿using RedRain.Slot;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public partial class UIManager : MonoBehaviour
{
	[Header("Bet Details Window")]
	[SerializeField] private GameObject betDetailsWindow;
	[SerializeField] private Button betPlusButton;
	[SerializeField] private Button betMinusButton;
	[SerializeField] private Button coinValuePlusButton;
	[SerializeField] private Button coinValueMinusButton;
	[SerializeField] private Button totalBetPlusButton;
	[SerializeField] private Button totalBetMinusButton;
	[SerializeField] private TextMeshProUGUI betMultiplierText;
	[SerializeField] private TextMeshProUGUI betText;
	[SerializeField] private TextMeshProUGUI coinValueText;
	[SerializeField] private TextMeshProUGUI totalBetText;
	private Button mainPullButton;

	public void SetBetMultiplierText(float betMultiplier) {
		betMultiplierText.text = $"Bet Multiplier x{betMultiplier}";
	}

	private void AdjustBet(BetDetail betDetail, bool isDoubleChance, float doubleChanceBetCost) {
		betText.text = betDetail.bet.ToString();
		coinValueText.text = betDetail.coinValue.ToString();
		totalBetText.text = betDetail.totalBet.ToString();
		mainBetText.text = $"Rp.{(isDoubleChance ? doubleChanceBetCost : betDetail.totalBet)},00";
		if (remainingAutoplayCount <= 0) {
			switch (betDetail.betIndexState) {
				case BetDetailIndexState.Minimum:
					betMinusButton.interactable = false;
					break;
				case BetDetailIndexState.InBetween:
					betMinusButton.interactable = true;
					betPlusButton.interactable = true;
					break;
				case BetDetailIndexState.Maximum:
					betPlusButton.interactable = false;
					break;
			}
			switch (betDetail.coinValueIndexState) {
				case BetDetailIndexState.Minimum:
					coinValueMinusButton.interactable = false;
					break;
				case BetDetailIndexState.InBetween:
					coinValueMinusButton.interactable = true;
					coinValuePlusButton.interactable = true;
					break;
				case BetDetailIndexState.Maximum:
					coinValuePlusButton.interactable = false;
					break;
			}
			switch (betDetail.totalBetIndexState) {
				case BetDetailIndexState.Minimum:
					totalBetMinusButton.interactable = false;
					totalBetMinusMainButton.interactable = false;
					totalBetPlusMainButton.interactable = true;
					break;
				case BetDetailIndexState.InBetween:
					totalBetMinusButton.interactable = true;
					totalBetMinusMainButton.interactable = true;
					totalBetPlusButton.interactable = true;
					totalBetPlusMainButton.interactable = true;
					break;
				case BetDetailIndexState.Maximum:
					totalBetPlusButton.interactable = false;
					totalBetPlusMainButton.interactable = false;
					totalBetMinusMainButton.interactable = true;
					break;
			}
		}
	}

	private void OpenBetDetailsWindow() {
		if (!betDetailsWindow.activeSelf) betDetailsWindow.SetActive(true);
	}
}
