﻿public enum ColumnState
{
	NotMoving = 0,
	StartMoving = 1,
	Moving = 2,
	SlowingDown = 3,
	Repeating = 4,
	Skipping = 5,
	Stopping = 6,
}