﻿public enum BetDetailIndexState
{
	Minimum = -1,
	InBetween = 0,
	Maximum = 1
}