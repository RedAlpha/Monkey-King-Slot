﻿public enum IconID {
	Wild = 0,
	JinguBar = 1,
	NineToothIronRake = 2,
	CrescentMoon = 3,
	SanZangStaff = 4,
	BaJie = 5,
	WuJing = 6,
	SanZang = 7,
	WuKong = 8,
	Scatter = 99,
	FullWild = 100,
}