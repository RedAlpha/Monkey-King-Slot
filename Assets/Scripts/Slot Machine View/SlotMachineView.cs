﻿using RedRain.Slot;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.U2D;

public class SlotMachineView : MonoBehaviour {
    [SerializeField] private SlotColumn[] slotColumns;
    [SerializeField] private SpriteAtlas iconAtlas;
    [SerializeField] private GameObject iconPrefab;
    [SerializeField] private GameObject animatedIconPrefab;

    public System.Action SlotAnimationFinishEvent;

    private List<SlotColumn> slowingColumns = new List<SlotColumn>();
    private List<IconID> randomizedIconPool = new List<IconID>();
    private int columnAnimationFinishedCount;
    private int startSlowingIndex;

    void Start() {
        randomizedIconPool = System.Enum.GetValues(typeof(IconID)).Cast<IconID>().ToList();
        randomizedIconPool.Remove(IconID.FullWild);
        for (int i = 0; i < slotColumns.Length; i++) {
            slotColumns[i].Initialize(iconAtlas, iconPrefab, animatedIconPrefab, randomizedIconPool, SlotSystem.RowCount[i], SlotSystem.SlotSpeed);
            slotColumns[i].ColumnAnimationFinishEvent += OnColumnAnimationFinished;
        }
    }

    public void BeginSlotAnimation(List<IconID> slotContent) {
        columnAnimationFinishedCount = 0;
        startSlowingIndex = 0;
        slowingColumns.Clear();
        int scatterCount = 0;
        for (int i = 0; i < slotColumns.Length; i++) {
            slotColumns[i].RemoveAnimationLayer();
            List<IconID> iconsInColumn = new List<IconID>();
            for (int j = 0; j < slotContent.Count; j += 5) {
                iconsInColumn.Add(slotContent[i + j]);
            }
            slotColumns[i].SetContent(iconsInColumn);
            if (scatterCount >= 2) {
                slowingColumns.Add(slotColumns[i]);
                slotColumns[i].SetRepeatValue(true);
                if (startSlowingIndex == 0) {
                    startSlowingIndex = i;
                }
            }
            foreach (IconID iconID in iconsInColumn) {
                if (iconID is IconID.Scatter) {
                    scatterCount++;
                    break;
                }
            }
        }
        StartCoroutine(BeginSequentialSlotAnimation(0.15f));
    }

    private IEnumerator BeginSequentialSlotAnimation(float waitTime) {
        for (int i = 0; i < slotColumns.Length; i++) {
            slotColumns[i].PlayColumnAnimation();
            yield return new WaitForSeconds(waitTime);
        }
    }

    public void SkipResult() {
        foreach (SlotColumn slotColumn in slotColumns) {
            slotColumn.Skip();
        }
    }

    public void PlayIconAnimations(List<MatchingResult> matchingResults) {
        List<int> matchingIndexes = new List<int>();
        foreach (MatchingResult matchingResult in matchingResults) {
            foreach (int newMatchingIndex in matchingResult.matchingIndexes) {
                if (!matchingIndexes.Contains(newMatchingIndex)) matchingIndexes.Add(newMatchingIndex);
            }
        }
        foreach (int matchingIndex in matchingIndexes) {
            int iconInColumnIndex = 0;
            int slotColumnIndex = matchingIndex;
            while (slotColumnIndex >= 5) {
                slotColumnIndex -= 5;
                iconInColumnIndex++;
            }
            slotColumns[slotColumnIndex].PlayIconAnimation(iconInColumnIndex);
        }
    }

    private void OnColumnAnimationFinished() {
        columnAnimationFinishedCount++;
        if (columnAnimationFinishedCount >= startSlowingIndex && slowingColumns.Count > 0) {
            slowingColumns[0].SlowDown();
            slowingColumns.RemoveAt(0);
            foreach (SlotColumn slotColumn in slowingColumns) {
                slotColumn.KeepRolling();
            }
        }
        if (columnAnimationFinishedCount >= slotColumns.Length) {
            SlotAnimationFinishEvent?.Invoke();
        }
    }
}
