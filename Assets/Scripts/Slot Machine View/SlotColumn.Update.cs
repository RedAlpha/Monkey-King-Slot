﻿using RedRain.Slot;
using UnityEngine;

public partial class SlotColumn : MonoBehaviour
{
    private Vector3 startPosition, targetPosition;
    private float nudgeTime, startTime, distance, distanceCovered;
    private bool postSlowdown = false;

    void Update() {
        switch (columnState) {
            case ColumnState.StartMoving:
                Move(-1f * SlotSystem.StartNudgeSpeedMultiplier);
                nudgeTime += Time.deltaTime;
                if (nudgeTime >= SlotSystem.StartNudgeTime) {
                    // Nudge upon start rolling
                    nudgeTime = 0;
                    columnState = isGoingToRepeat ? ColumnState.Repeating : ColumnState.Moving;
                    isGoingToRepeat = false;
                }
                break;
            case ColumnState.Moving:
            case ColumnState.Skipping:
                Move(scrollingPart.position.y > 32f ? 1f : 0.35f);
                break;
            case ColumnState.Repeating:
                Move(1f);
                break;
            case ColumnState.SlowingDown:
                Move(scrollingPart.position.y / startMovementYPosition + 0.05f);
                break;
            case ColumnState.Stopping:
                distanceCovered = (Time.time - startTime) * SlotSystem.StopNudgeLerpSpeed * (postSlowdown ? SlotSystem.StopNudgeSpeedMultiplier : 1f);
                scrollingPart.position = Vector3.Lerp(startPosition, targetPosition, distanceCovered / distance);
                if (scrollingPart.position.y >= transform.position.y) {
                    // Literally Stop
                    columnState = ColumnState.NotMoving;
                    scrollingPart.position = new Vector3(scrollingPart.position.x, transform.position.y, scrollingPart.position.z);
                    fullBorder.SetActive(false);
                    ColumnAnimationFinishEvent?.Invoke();
                }
                break;
        }
        if (columnState != ColumnState.NotMoving && columnState != ColumnState.StartMoving && scrollingPart.position.y <= -1f * SlotSystem.StopNudgeLerpDistance) {
            if (columnState == ColumnState.Repeating) {
                ResetPosition();
            } else {
                // Begin to stop
                startTime = Time.time;
                startPosition = scrollingPart.position;
                targetPosition = new Vector3(scrollingPart.position.x, startMovementYPosition, scrollingPart.position.z);
                distance = Vector3.Distance(startPosition, targetPosition);
                postSlowdown = columnState == ColumnState.SlowingDown ? true : false;
                columnState = ColumnState.Stopping;
            }
        }
    }

    private void Move(float speedMultiplier) {
        scrollingPart.Translate(Vector3.down * speed * speedMultiplier * Time.deltaTime);
    }
}
