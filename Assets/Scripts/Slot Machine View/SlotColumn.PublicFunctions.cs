﻿using System.Collections.Generic;
using UnityEngine;

public partial class SlotColumn : MonoBehaviour
{
    private bool isPlayingFullWildAnimation = false;

    public void SetContent(List<IconID> iconSequence) {
        storedIcons = new List<IconID>(iconSequence);
    }

    public void SetRepeatValue(bool value) {
        isGoingToRepeat = value;
    }

    public void PlayColumnAnimation() {
        // MK-Specific
        if (isPlayingFullWildAnimation) {
            fullWildSpineAnimation.AnimationState.SetAnimation(0, "Exit", false);
        }
        isPlayingFullWildAnimation = false;
        for (int i = 0; i < icons.Count; i++) {
            icons[i].SetSprite(randomizedIconPool[Random.Range(0, randomizedIconPool.Count)]);
        }
        for (int i = 0; i < displayedIcons.Count; i++) {
            displayedIcons[i].SetSprite(storedIcons[i]);
            // MK-Specific
            if (!isPlayingFullWildAnimation && storedIcons[i] == IconID.FullWild) {
                isPlayingFullWildAnimation = true;
                fullWildMeshRenderer.enabled = true;
                fullWildSpineAnimation.AnimationState.SetAnimation(0, "Win", false);
            }
        }
        for (int i = 0; i < lastResultIcons.Count; i++) {
            lastResultIcons[i].SetSprite(lastResultIconID[i]);
        }
        for (int i = 0; i < animatedIcons.Count; i++) {
            animatedIcons[i].SetID(storedIcons[i]);
        }
        lastResultIconID.Clear();
        for (int i = 1; i < shownRowCount + 1; i++) {
            lastResultIconID.Add(storedIcons[storedIcons.Count - i]);
        }
        ResetPosition();
        columnState = ColumnState.StartMoving;
    }

    public void Skip() {
        if (columnState == ColumnState.NotMoving) return;
        columnState = ColumnState.Skipping;
        fullBorder.SetActive(false);
        scrollingPart.position = new Vector3(transform.position.x, 17f, transform.position.z);
    }

    public void SlowDown() {
        if (columnState == ColumnState.Skipping || columnState == ColumnState.NotMoving || columnState == ColumnState.Stopping) return;
        columnState = ColumnState.SlowingDown;
        fullBorder.SetActive(true);
        ResetPosition();
    }

    public void KeepRolling() {
        if (columnState == ColumnState.Skipping || columnState == ColumnState.NotMoving || columnState == ColumnState.Stopping) return;
        columnState = ColumnState.Repeating;
        ResetPosition();
    }

    public void PlayIconAnimation(int index) {
        animatedIcons[index].SetAndPlayAnimation();
        icons[index].ToggleSpriteRenderer(false);
    }

    public void RemoveAnimationLayer() {
        foreach (AnimatedIcon animatedIcon in animatedIcons) {
            animatedIcon.TurnOff();
        }
        foreach (Icon icon in icons) {
            icon.ToggleSpriteRenderer(true);
        }
    }
}
