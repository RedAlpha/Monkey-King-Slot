﻿using Spine.Unity;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public partial class SlotColumn : MonoBehaviour
{
    [SerializeField] private Transform scrollingPart;
    [SerializeField] private Transform iconParent;
    [SerializeField] private Transform animatedIconParent;
    [SerializeField] private GameObject fullBorder;
    [SerializeField] private SkeletonAnimation fullWildSpineAnimation;
    private MeshRenderer fullWildMeshRenderer;

    public System.Action ColumnAnimationFinishEvent;

    private const int shownRowCount = 5;
    private const float verticalDifference = 7f;
    private List<Icon> icons = new List<Icon>();
    private List<Icon> displayedIcons = new List<Icon>();
    private List<Icon> lastResultIcons = new List<Icon>();
    private List<AnimatedIcon> animatedIcons = new List<AnimatedIcon>();
    private List<IconID> storedIcons = new List<IconID>();
    private List<IconID> randomizedIconPool = new List<IconID>();
    private List<IconID> lastResultIconID = new List<IconID>();
    private ColumnState columnState = ColumnState.NotMoving;
    private bool isGoingToRepeat = false;
    private float startMovementYPosition;
    private float speed;

    public void Initialize(SpriteAtlas iconAtlas, GameObject iconPrefab, GameObject animatedIconPrefab, List<IconID> randomizedIconPool, int rowCount, float speed) {
        this.randomizedIconPool = new List<IconID>(randomizedIconPool);
        this.speed = speed;
        fullWildMeshRenderer = fullWildSpineAnimation.GetComponent<MeshRenderer>();
        for (int i = 0; i < rowCount; i++) {
            CreateNewIcon(iconPrefab, iconAtlas, i);
        }
        for (int i = 0; i < shownRowCount; i++) {
            icons[i].name = $"Target Displayed Icon {i}";
            displayedIcons.Add(icons[i]);
        }
        for (int i = icons.Count - 1; i > icons.Count - shownRowCount - 1; i--) {
            icons[i].name = $"Last Result Icon {i}";
            lastResultIcons.Add(icons[i]);
        }
        CreateNewIcon(iconPrefab, iconAtlas, rowCount);
        CreateNewIcon(iconPrefab, iconAtlas, -1);
        for (int i = 1; i < shownRowCount + 1; i++) {
            lastResultIconID.Add(randomizedIconPool[Random.Range(0, randomizedIconPool.Count)]);
        }
        for (int i = 0; i < lastResultIcons.Count; i++) {
            lastResultIcons[i].SetSprite(lastResultIconID[i]);
        }
        for (int i = 0; i < shownRowCount; i++) {
            AnimatedIcon animatedIcon = Instantiate(animatedIconPrefab, new Vector3(scrollingPart.position.x, scrollingPart.position.y + (verticalDifference * 2) - verticalDifference * i, scrollingPart.position.z), Quaternion.identity, animatedIconParent).GetComponent<AnimatedIcon>();
            animatedIcon.Initialize();
            animatedIcons.Add(animatedIcon);
        }
        startMovementYPosition = rowCount * verticalDifference - (verticalDifference * 5) + transform.position.y;
        ResetPosition();
    }

    private void ResetPosition() {
        scrollingPart.position = new Vector3(scrollingPart.position.x, startMovementYPosition, scrollingPart.position.z);
    }

    private Icon CreateNewIcon(GameObject iconPrefab, SpriteAtlas iconAtlas, int row) {
        Icon newIcon = Instantiate(iconPrefab, new Vector3(scrollingPart.position.x, scrollingPart.position.y + (verticalDifference * 2) - verticalDifference * row, scrollingPart.position.z), Quaternion.identity, iconParent).GetComponent<Icon>();
        newIcon.Initialize(iconAtlas);
        icons.Add(newIcon);
        return newIcon;
    }
}
