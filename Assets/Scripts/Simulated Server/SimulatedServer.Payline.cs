﻿using System.Collections.Generic;

namespace RedRain.Slot {
	public partial class SimulatedServer
	{
		private List<MatchingResult> GetMatchingResult(List<IconID> slotContent) {
			List<MatchingResult> matchingResults = new List<MatchingResult>();
			List<int> matchingSlotIndexes = new List<int>();
			// Loop through all the icon IDs
			for (int i = 1; i < System.Enum.GetValues(typeof(IconID)).Length - 2; i++) {
				// For every icon ID loop through the entire content of the slot machine downward for each column starting from the leftmost
				for (int j = 0; j < 5; j++) {
					for (int k = j; k < slotContent.Count; k += 5) {
						if ((int)slotContent[k] == i || slotContent[k] == IconID.Wild || slotContent[k] == IconID.FullWild) {
							matchingSlotIndexes.Add(k);
						}
					}
				}
				// Loop through the payline to see if this icon is in the specific slot index 
				List<int> matchingIndexes = new List<int>();
				for (int j = 0; j < Paylines.Count; j++) {
					matchingIndexes.Clear();
					foreach (int slotIndex in matchingSlotIndexes) {
						for (int k = 0; k < Paylines[j].Count; k++) {
							if (slotIndex == Paylines[j][k]) {
								matchingIndexes.Add(slotIndex);
							}
						}
					}
					if (matchingIndexes.Count >= 3) {
						List<int> storedIndexes = new List<int>(ProcessIndexes(matchingIndexes, matchingResults));
						if (storedIndexes.Count >= 3) matchingResults.Add(new MatchingResult((IconID)i, storedIndexes));
					}
				}
				matchingSlotIndexes.Clear();
			}
			int scatterCount = 0;
			foreach (IconID iconID in slotContent) {
				if (iconID is IconID.Scatter) {
					scatterCount++;
				}
			}
			if (scatterCount >= 3) {
				List<int> storedIndexes = new List<int>();
				for (int i = 0; i < slotContent.Count; i++) {
					if (slotContent[i] == IconID.Scatter) storedIndexes.Add(i);
				}
				matchingResults.Add(new MatchingResult(IconID.Scatter, storedIndexes));
			}
			return matchingResults;
		}

		private List<int> ProcessIndexes(List<int> incomingMatchingIndexes, List<MatchingResult> storedMatchingResults) {
			List<int> temporaryIndexes = new List<int>();
			if (incomingMatchingIndexes[0] % 5 == 0) {
				temporaryIndexes.Add(incomingMatchingIndexes[0]);
				int currentColumn = 0;
				foreach (int matchingIndex in incomingMatchingIndexes) {
					if (matchingIndex == incomingMatchingIndexes[0]) continue;
					if (matchingIndex % 5 == currentColumn + 1) {
						temporaryIndexes.Add(matchingIndex);
						currentColumn++;
					} else {
						temporaryIndexes.Clear();
						break;
					}
				}
			}
			foreach (MatchingResult storedMatchingResult in storedMatchingResults) {
				if (temporaryIndexes.Count < storedMatchingResult.matchingIndexes.Count) {
					int checkAmount = 0;
					for (int i = 0; i < temporaryIndexes.Count; i++) {
						if (temporaryIndexes[i] == storedMatchingResult.matchingIndexes[i]) {
							checkAmount++;
							if (checkAmount >= temporaryIndexes.Count) {
								temporaryIndexes.Clear();
								break;
							}
						}
					}
				} else if (temporaryIndexes.Count > storedMatchingResult.matchingIndexes.Count) {
					int checkAmount = 0;
					for (int i = 0; i < storedMatchingResult.matchingIndexes.Count; i++) {
						if (temporaryIndexes[i] == storedMatchingResult.matchingIndexes[i]) {
							checkAmount++;
							if (checkAmount >= storedMatchingResult.matchingIndexes.Count) {
								storedMatchingResult.matchingIndexes = new List<int>(temporaryIndexes);
								temporaryIndexes.Clear();
								return temporaryIndexes;
							}
						}
					}
				} else if (temporaryIndexes.Count == storedMatchingResult.matchingIndexes.Count) {
					int checkAmount = 0;
					for (int k = 0; k < storedMatchingResult.matchingIndexes.Count; k++) {
						if (temporaryIndexes[k] == storedMatchingResult.matchingIndexes[k]) {
							checkAmount++;
							if (checkAmount >= storedMatchingResult.matchingIndexes.Count) {
								temporaryIndexes.Clear();
								break;
							}
						}
					}
				}
			}
			return temporaryIndexes;
		}
	}
}
