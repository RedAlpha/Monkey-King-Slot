﻿using SimpleJSON;
using UnityEngine;

namespace RedRain.Slot {
    public partial class SimulatedServer {
		//Game Statistics
		private readonly JSONArray Paylines = JSON.Parse(Resources.Load<TextAsset>("Data/Paylines").ToString())["Paylines"].AsArray;
		private readonly JSONArray BetRange = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["BetRange"].AsArray;
		private readonly JSONArray CoinValue = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["CoinValue"].AsArray;
		private readonly bool ActivateRTP = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["ActivateRTP"];
		private readonly int HitFrequency = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["HitFrequency"];
		private readonly int RTP = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["RTP"];
		private readonly int GridCount = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["GridCount"];
		private readonly int FullWildChance = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["FullWildChance"];
		private readonly int BetMultiplier = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["BetMultiplier"];
		private readonly float FreeSpinMultiplier = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["FreeSpinMultiplier"];
		private readonly float AnteBetMultiplier = JSON.Parse(Resources.Load<TextAsset>("Data/Server Data").ToString())["AnteBetMultiplier"];
		private PullType nextPullType;
		private float currentPlayerBet;
		private float credit;

		//Shared Across All Same Games
		private float TotalLosing;
	}
}