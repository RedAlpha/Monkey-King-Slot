﻿using System.Collections.Generic;

namespace RedRain.Slot {
	public partial class SimulatedServer
	{
		public List<Bet> betList = new List<Bet>();
		public int betIndex;
		public int coinValueIndex;
		public int totalBetIndex;
		private float minTotalBetValue, maxTotalBetValue;

		private BetDetail DecreaseBet() {
			betIndex--;
			if (betIndex < 0) {
				betIndex = 0;
			}
			return new BetDetail(BetRange[betIndex], CoinValue[coinValueIndex], CalculateCurrentBet(), GetBetIndexState(), GetCoinValueIndexState(), GetTotalBetIndexState());
		}

		private BetDetail IncreaseBet() {
			betIndex++;
			if (betIndex >= BetRange.Count) {
				betIndex = BetRange.Count - 1;
			}
			return new BetDetail(BetRange[betIndex], CoinValue[coinValueIndex], CalculateCurrentBet(), GetBetIndexState(), GetCoinValueIndexState(), GetTotalBetIndexState());
		}

		private BetDetail DecreaseCoinValue() {
			coinValueIndex--;
			if (coinValueIndex < 0) {
				coinValueIndex = 0;
			}
			return new BetDetail(BetRange[betIndex], CoinValue[coinValueIndex], CalculateCurrentBet(), GetBetIndexState(), GetCoinValueIndexState(), GetTotalBetIndexState());
		}

		private BetDetail IncreaseCoinValue() {
			coinValueIndex++;
			if (coinValueIndex >= CoinValue.Count) {
				coinValueIndex = CoinValue.Count - 1;
			}
			return new BetDetail(BetRange[betIndex], CoinValue[coinValueIndex], CalculateCurrentBet(), GetBetIndexState(), GetCoinValueIndexState(), GetTotalBetIndexState());
		}

		private BetDetail DecreaseTotalBet() {
			for (int i = betList.Count - 1; i >= 0; i--) {
				if (betList[i].totalBetValue < currentPlayerBet) {
					totalBetIndex = i;
					betIndex = betList[totalBetIndex].betIndex;
					coinValueIndex = betList[totalBetIndex].coinValueIndex;
					currentPlayerBet = betList[i].totalBetValue;
					break;
				}
			}
			return new BetDetail(BetRange[betIndex], CoinValue[coinValueIndex], currentPlayerBet, GetBetIndexState(), GetCoinValueIndexState(), GetTotalBetIndexState());
		}

		private BetDetail IncreaseTotalBet() {
			for (int i = 0; i < betList.Count; i++) {
				if (betList[i].totalBetValue > currentPlayerBet) {
					totalBetIndex = i;
					betIndex = betList[totalBetIndex].betIndex;
					coinValueIndex = betList[totalBetIndex].coinValueIndex;
					currentPlayerBet = betList[i].totalBetValue;
					break;
				}
			}
			return new BetDetail(BetRange[betIndex], CoinValue[coinValueIndex], currentPlayerBet, GetBetIndexState(), GetCoinValueIndexState(), GetTotalBetIndexState());
		}

		private float CalculateCurrentBet() {
			currentPlayerBet = BetMultiplier * BetRange[betIndex] * CoinValue[coinValueIndex];
			return currentPlayerBet;
		}

		private BetDetailIndexState GetBetIndexState() {
			BetDetailIndexState betDetailIndexState = BetDetailIndexState.InBetween;
			if (betIndex <= 0) {
				betDetailIndexState = BetDetailIndexState.Minimum;
			} else if (betIndex >= BetRange.Count - 1) {
				betDetailIndexState = BetDetailIndexState.Maximum;
			}
			return betDetailIndexState;
		}

		private BetDetailIndexState GetCoinValueIndexState() {
			BetDetailIndexState betDetailIndexState = BetDetailIndexState.InBetween;
			if (coinValueIndex <= 0) {
				betDetailIndexState = BetDetailIndexState.Minimum;
			} else if (coinValueIndex >= CoinValue.Count - 1) {
				betDetailIndexState = BetDetailIndexState.Maximum;
			}
			return betDetailIndexState;
		}

		private BetDetailIndexState GetTotalBetIndexState() {
			BetDetailIndexState betDetailIndexState = BetDetailIndexState.InBetween;
			if (currentPlayerBet <= minTotalBetValue) {
				betDetailIndexState = BetDetailIndexState.Minimum;
			} else if (currentPlayerBet >= maxTotalBetValue) {
				betDetailIndexState = BetDetailIndexState.Maximum;
			}
			return betDetailIndexState;
		}
	}
}
