﻿using System.Collections.Generic;
using UnityEngine;

namespace RedRain.Slot {
	public partial class SimulatedServer {
		private SlotResult currentSlotResult;
		private float thisRoundWinning;

		private void Pull(bool isUsingBuyFreeSpin) {
			thisRoundWinning = 0;
			float betUsed = isUsingBuyFreeSpin ? currentPlayerBet * 100 : currentPlayerBet;
			TotalLosing += betUsed;
			credit -= betUsed;
			List<IconID> slotContent = GetSlotResult(isUsingBuyFreeSpin, false);
			SendToClientEvent_Pull?.Invoke(slotContent, PullType.Normal, credit - thisRoundWinning);
			CheckFreeSpinOnPull(slotContent);
		}

		private List<IconID> GetSlotResult(bool isUsingBuyFreeSpin, bool isFreeSpin) {
			SlotResult simulatedResult = null;
			float potentialWinning = TotalLosing;
			if (ActivateRTP) {
				while (potentialWinning / TotalLosing * 100 > RTP) {
					simulatedResult = SimulateSlotResult(isUsingBuyFreeSpin, isFreeSpin);
					potentialWinning = CalculatePotentialWinning(simulatedResult);
				}
			} else {
				simulatedResult = SimulateSlotResult(isUsingBuyFreeSpin, isFreeSpin);
				potentialWinning = CalculatePotentialWinning(simulatedResult);
			}
			currentSlotResult = simulatedResult;
			credit += potentialWinning;
			thisRoundWinning = potentialWinning;
			return currentSlotResult.slotContent;
		}

		private SlotResult SimulateSlotResult(bool isUsingBuyFreeSpin, bool isFreeSpin) {
			List<IconID> slotContent = new List<IconID>();
			for (int i = 0; i < GridCount; i++) {
				slotContent.Add(GetRandomIconForSlot(isFreeSpin));
			}
			if (!isFreeSpin) {
#if UNITY_EDITOR
				RigSlotResult(slotContent, ref slotContent);
#endif
				CheckScatter(slotContent, ref slotContent);
			}
			if (isUsingBuyFreeSpin) {
				slotContent[4] = IconID.Scatter;
				slotContent[10] = IconID.Scatter;
				slotContent[22] = IconID.Scatter;
			} else if (isFreeSpin) {
				int fullWildCount = 0;
				bool[] isFullWild = new bool[5];
				for (int i = 0; i < 5; i++) {
					if (fullWildCount >= 4) break;
					if (Random.Range(0, 100) < FullWildChance) {
						isFullWild[i] = true;
						fullWildCount++;
					}
				}
				for (int i = 0; i < 5; i++) {
					if (!isFullWild[i]) continue;
					for (int j = 0; j < slotContent.Count; j += 5) {
						slotContent[i + j] = IconID.FullWild;
					}
				}
			}
			return new SlotResult(slotContent, GetMatchingResult(slotContent));
		}

		private void RigSlotResult(List<IconID> slotContent, ref List<IconID> result) { //RIGGED SLOT RESULT, DEBUG ONLY
			result = new List<IconID>(slotContent);
#if UNITY_EDITOR
			//result[0] = IconID.Scatter;
			//result[1] = IconID.Scatter;
			//result[2] = IconID.Scatter;
#endif
		}

		private void CheckScatter(List<IconID> slotContent, ref List<IconID> result) {
			result = new List<IconID>(slotContent);
			for (int i = 0; i < 5; i++) {
				bool isScatterPresentInColumn = false;
				for (int j = i; j < result.Count; j += 5) {
					if (result[j] == IconID.Scatter) {
						if (isScatterPresentInColumn) {
							result[j] = IconID.Wild;
						} else {
							isScatterPresentInColumn = true;
						}
					}
				}
			}
		}

		private float CalculatePotentialWinning(SlotResult simulatedResult) {
			float calculatedPotentialWinning = 0;
			foreach (MatchingResult matchingResult in simulatedResult.matchingResults) {
				calculatedPotentialWinning += currentPlayerBet * GetIconMultiplier(matchingResult.iconID, matchingResult.matchingIndexes.Count);
			}
			return calculatedPotentialWinning;
		}

		private float GetIconMultiplier(IconID iconID, int index) {
			int[] intArray = IconData.GetIntArray(iconID, IconStatus.BetMultiplier);
			if (index > intArray.Length) index = intArray.Length;
			return intArray[index - 1] / 100f;
		}

		private IconID GetRandomIconForSlot(bool isFreeSpin) {
			int usedTotalFrequency = normalTotalFrequency;
			if (isDoubleChance) {
				usedTotalFrequency = anteBetTotalFrequency;
			} else if (isFreeSpin) {
				usedTotalFrequency = freeSpinTotalFrequency;
			}
			int randomInt = Random.Range(0, usedTotalFrequency);
			foreach (IconID icon in slotIconList) {
				int currentFrequency = IconData.GetInt(icon, isFreeSpin ? IconStatus.FreeSpinFrequency : IconStatus.DefaultFrequency);
				if (currentFrequency <= 0) continue;
				if (isDoubleChance && icon == IconID.Scatter) currentFrequency *= 2;
				if (randomInt < currentFrequency) {
					return icon;
				}
				randomInt -= currentFrequency;
			}
			return IconID.Wild;
		}
	}
}
