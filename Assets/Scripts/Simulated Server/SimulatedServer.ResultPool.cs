﻿using System.Collections.Generic;
using System.Linq;

namespace RedRain.Slot {
	public partial class SimulatedServer
	{
		private List<IconID> slotIconList = new List<IconID>();
		private int normalTotalFrequency;
		private int freeSpinTotalFrequency;
		private int anteBetTotalFrequency;

		private void SetupResultIconPool() {
			// Normal Pool
			slotIconList = System.Enum.GetValues(typeof(IconID)).Cast<IconID>().ToList();
			slotIconList.Remove(IconID.FullWild);
			foreach (IconID icon in slotIconList) {
				normalTotalFrequency += IconData.GetInt(icon, IconStatus.DefaultFrequency);
			}
			// Free Spin Pool
			foreach (IconID icon in slotIconList) {
				freeSpinTotalFrequency += IconData.GetInt(icon, IconStatus.FreeSpinFrequency);
			}
			// Ante Bet Pool
			anteBetTotalFrequency = normalTotalFrequency + IconData.GetInt(IconID.Scatter, IconStatus.DefaultFrequency);
		}
	}
}
