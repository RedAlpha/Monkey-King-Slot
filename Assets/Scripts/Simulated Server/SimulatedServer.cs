﻿using System.Linq;

namespace RedRain.Slot {
	public partial class SimulatedServer
	{
		public void InitializeServer() {
			SetupVariables();
			SetupResultIconPool();
		}

		public void SetupMockData() {
			betIndex = 0;
			coinValueIndex = 0;
			totalBetIndex = 0;
			credit = 1000000;
			currentPlayerBet = BetMultiplier * BetRange[betIndex] * CoinValue[coinValueIndex];
			InvokeBetDataSend(new BetDetail(BetRange[betIndex], CoinValue[coinValueIndex], currentPlayerBet, GetBetIndexState(), GetCoinValueIndexState(), GetTotalBetIndexState()));
			SendToClientEvent_InitializationData?.Invoke(BetMultiplier);
		}

		private void SetupVariables() {
			for (int i = 0; i < BetRange.Count; i++) {
				for (int j = 0; j < CoinValue.Count; j++) {
					betList.Add(new Bet(i, j, BetMultiplier * BetRange[i] * CoinValue[j]));
				}
			}
			betList = betList.OrderBy(w => w.totalBetValue).ToList();
			minTotalBetValue = betList[0].totalBetValue;
			maxTotalBetValue = betList[betList.Count - 1].totalBetValue;
		}
	}
}
