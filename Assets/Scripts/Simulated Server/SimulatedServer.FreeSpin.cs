﻿using System.Collections.Generic;

namespace RedRain.Slot {
	public partial class SimulatedServer
	{
		private int freeSpinRemaining = 0;

		private void CheckFreeSpinOnPull(List<IconID> slotContent) {
			int scatterCount = 0;
			foreach (IconID iconID in slotContent) {
				if (iconID is IconID.Scatter) {
					scatterCount++;
				}
			}
			if (scatterCount >= 3) {
				freeSpinRemaining = 10;
			}
			if (freeSpinRemaining > 0) {
				nextPullType = PullType.FreeSpin;
			} else {
				nextPullType = PullType.Normal;
			}
		}

		private void CheckFreeSpinOnAnimationFinished() {
			freeSpinRemaining--;
			thisRoundWinning = 0;
			if (freeSpinRemaining > 0) {
				SendToClientEvent_Pull?.Invoke(GetSlotResult(false, true), PullType.FreeSpin, credit - thisRoundWinning);
				if (freeSpinRemaining == 1) {
					nextPullType = PullType.Normal;
				} else {
					nextPullType = PullType.FreeSpin;
				}
			}
		}
	}
}
