﻿using System;
using System.Collections.Generic;

namespace RedRain.Slot {
	public partial class SimulatedServer
	{
		public Action<float> SendToClientEvent_InitializationData;
		public Action<List<IconID>, PullType, float> SendToClientEvent_Pull;
		public Action<List<MatchingResult>, PullType, float> SendToClientEvent_SlotAnimationFinish;
		public Action<BetDetail, float, float, float> SendToClientEvent_BetData;
		public Action<bool, float, float> SendToClientEvent_ToggleDoubleChance;

		private bool isDoubleChance;

		public void ClientPull() {
			Pull(false);
		}

		public void ClientSlotAnimationFinished() {
			InvokeBetDataSend(new BetDetail(BetRange[betIndex], CoinValue[coinValueIndex], currentPlayerBet, GetBetIndexState(), GetCoinValueIndexState(), GetTotalBetIndexState()));
			SendToClientEvent_SlotAnimationFinish?.Invoke(currentSlotResult.matchingResults, nextPullType, thisRoundWinning);
			CheckFreeSpinOnAnimationFinished();
		}

		public void ClientDecreaseBet() {
			InvokeBetDataSend(DecreaseBet());
		}

		public void ClientIncreaseBet() {
			InvokeBetDataSend(IncreaseBet());
		}

		public void ClientDecreaseCoinValue() {
			InvokeBetDataSend(DecreaseCoinValue());
		}

		public void ClientIncreaseCoinValue() {
			InvokeBetDataSend(IncreaseCoinValue());
		}

		public void ClientDecreaseTotalBet() {
			InvokeBetDataSend(DecreaseTotalBet());
		}

		public void ClientIncreaseTotalBet() {
			InvokeBetDataSend(IncreaseTotalBet());
		}

		public void ClientBuyFreeSpin() {
			Pull(true);
		}

		public void ClientToggleDoubleChance(bool isDoubleChance) {
			this.isDoubleChance = isDoubleChance;
			SendToClientEvent_ToggleDoubleChance?.Invoke(isDoubleChance, currentPlayerBet, currentPlayerBet * AnteBetMultiplier);
		}

		public void ClientRequestingSyncData() {
			InvokeBetDataSend(new BetDetail(BetRange[betIndex], CoinValue[coinValueIndex], currentPlayerBet, GetBetIndexState(), GetCoinValueIndexState(), GetTotalBetIndexState()));
		}

		private void InvokeBetDataSend(BetDetail betDetail) {
			SendToClientEvent_BetData?.Invoke(betDetail, betDetail.totalBet * FreeSpinMultiplier, betDetail.totalBet * AnteBetMultiplier, credit);
		}
	}
}
