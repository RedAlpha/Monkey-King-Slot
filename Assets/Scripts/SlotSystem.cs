﻿namespace RedRain.Slot {
    public static partial class SlotSystem
	{
		public static SimulatedServer SimulatedServer;

		public static void InitializeSystem() {
			SimulatedServer = new SimulatedServer();
			SimulatedServer.InitializeServer();
		}
	}
}