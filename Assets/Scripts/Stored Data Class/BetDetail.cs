﻿namespace RedRain.Slot {
    public class BetDetail
    {
        public int bet;
        public float coinValue;
        public float totalBet;
        public BetDetailIndexState betIndexState;
        public BetDetailIndexState coinValueIndexState;
        public BetDetailIndexState totalBetIndexState;

        public BetDetail(int bet, float coinValue, float totalBet, BetDetailIndexState betIndexState, BetDetailIndexState coinValueIndexState, BetDetailIndexState totalBetIndexState) {
            this.bet = bet;
            this.coinValue = coinValue;
            this.betIndexState = betIndexState;
            this.coinValueIndexState = coinValueIndexState;
            this.totalBetIndexState = totalBetIndexState;
            this.totalBet = totalBet;
        }
    }
}