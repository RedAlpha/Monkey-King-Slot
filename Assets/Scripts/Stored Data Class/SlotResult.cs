﻿using System.Collections.Generic;

namespace RedRain.Slot {
    public class SlotResult {

        public List<IconID> slotContent;
        public List<MatchingResult> matchingResults;

        public SlotResult(List<IconID> slotContent, List<MatchingResult> matchingResults) {
            this.slotContent = new List<IconID>(slotContent);
            this.matchingResults = new List<MatchingResult>(matchingResults);
        }
    }

    public class MatchingResult {

        public IconID iconID;
        public List<int> matchingIndexes;

        public MatchingResult(IconID iconID, List<int> matchingIndexes) {
            this.iconID = iconID;
            this.matchingIndexes = new List<int>(matchingIndexes);
        }
    }
}