﻿namespace RedRain.Slot {
    public class Bet {
        public int betIndex;
        public int coinValueIndex;
        public float totalBetValue;

        public Bet(int betIndex, int coinValueIndex, float totalBetValue) {
            this.betIndex = betIndex;
            this.coinValueIndex = coinValueIndex;
            this.totalBetValue = totalBetValue;
        }
    }
}